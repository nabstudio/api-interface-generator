ARG NODE_VERSION="lts"


# ===== Builder =====
FROM node:${NODE_VERSION}-alpine as builder

WORKDIR /app

RUN \
  apk add \
  --no-cache \
  --update \
  --virtual "shared-dependencies" \
  "bash" \
  "bc" \
  "ca-certificates" \
  "curl" \
  "jq"

RUN curl -s "https://raw.githubusercontent.com/envkey/envkey-source/master/install.sh" | bash

# Core
COPY "./.bin" "./.bin"
COPY "./lib/bash/core.sh" "./lib/bash/core.sh"
COPY "./lib/bash/node.sh" "./lib/bash/node.sh"

RUN \
  apk add \
  --virtual "build-dependencies" \
  "alpine-sdk" \
  "build-base" \
  "gcc" \
  "git" \
  "go" \
  "libc-dev" \
  "make" \
  "openssh-client" \
  "python" \
  "wget"

RUN curl -sfL "https://install.goreleaser.com/github.com/tj/node-prune.sh" | bash

# SSH
COPY "./.ssh/id_rsa" "./.ssh/id_rsa"
RUN mv -f "./.ssh" "${HOME}/.ssh"
RUN chmod 600 "${HOME}/.ssh/id_rsa"
RUN ssh-keyscan "bitbucket.org" > "${HOME}/.ssh/known_hosts"

# Install
COPY "./.node-version" "./.node-version"
COPY "./package.json" "./package.json"
COPY "./package-lock.json" "./package-lock.json"
COPY "./patches" "./patches"
COPY "./pipeline/install" "./pipeline/install"
RUN ./pipeline/install

# Build
COPY "./tsconfig.json" "./tsconfig.json"
COPY "./src" "./src"
COPY "./pipeline/build" "./pipeline/build"
RUN ./pipeline/build

# Install (Production)
RUN rm -rf "./node_modules"
RUN ./pipeline/install --production

# Prune
RUN ./bin/node-prune "./node_modules"


# ===== Production =====
FROM node:${NODE_VERSION}-alpine

WORKDIR /app

RUN \
  apk add \
  --no-cache \
  --update \
  --virtual "shared-dependencies" \
  "bash" \
  "bc" \
  "ca-certificates" \
  "curl" \
  "jq"

RUN curl -s "https://raw.githubusercontent.com/envkey/envkey-source/master/install.sh" | bash

# Core
COPY "./.bin" "./.bin"
COPY "./lib/bash/core.sh" "./lib/bash/core.sh"
COPY "./lib/bash/node.sh" "./lib/bash/node.sh"

# Run
COPY "./.node-version" "./.node-version"
COPY "./package.json" "./package.json"
COPY "./package-lock.json" "./package-lock.json"
COPY "./pipeline/run" "./pipeline/run"

# Built
COPY --from=builder "/app/node_modules" "./node_modules"
COPY --from=builder "/app/build" "./build"

ENTRYPOINT [ \
  "./pipeline/run" \
  ]
