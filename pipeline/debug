#!/usr/bin/env bash

source "./lib/bash/core.sh"
source "./lib/bash/node.sh"

function main() {

  local COMMAND="${1:-}"

  if [[ -n "${COMMAND}" && "${COMMAND}" != "-"* ]]; then
    if ! jq --exit-status ".scripts.\"${COMMAND}\"" "./package.json" >/dev/null; then
      echo "Script '${COMMAND}' not found." >&2
      exit 1
    fi
    shift
  else
    if [[ "${COMMAND}" == "--" ]]; then
      shift
    fi
    COMMAND=""
  fi

  if [[ -z "${COMMAND}" ]]; then
    COMMAND="dev"
  fi

  if [[ ! -d "./node_modules" ]]; then
    "./pipeline/install"
  fi

  echo -e "${OK_COLOR}==> Debugging app..."

  loadEnvKey

  export NODE_DEBUG_PORT="${NODE_DEBUG_PORT:-9229}"
  echo "Cleaning up port ${NODE_DEBUG_PORT} for debugging..."
  ( 
    # set -x
    (lsof -t -i ":${NODE_DEBUG_PORT}" || true) |
      xargs kill
  )

  # Kill old processes
  echo "Cleaning up old nodemon processes..."
  # shellcheck disable=SC2009
  ps |
    grep --invert-match "grep" |
    (grep "nodemon" || true) |
    (grep "${PWD}" || true) |
    awk '{print $1}' |
    xargs kill

  NPM="$(command -v npm)"

  (
    set -x
    "${NPM}" run "${COMMAND[@]}" "$@"
  )

}

main "$@"
