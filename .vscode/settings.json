{
  // Debug
  "debug.node.autoAttach": "on",

  // Explorer
  "explorer.confirmDragAndDrop": false,

  // Editor - Format
  "editor.formatOnPaste": true,
  "editor.formatOnSave": true,

  // Editor - Indentation
  "editor.detectIndentation": true,
  "editor.insertSpaces": true,
  "editor.tabSize": 2,

  // Editor - Rulers
  "editor.rulers": [120],

  // Git
  "git.autofetch": true,
  "git.confirmSync": false,

  // Git Diff Editor
  "diffEditor.ignoreTrimWhitespace": false,

  // Terminal
  "terminal.integrated.scrollback": 1000000000,
  "terminal.integrated.rendererType": "auto",

  // Extensions
  "extensions.ignoreRecommendations": false,

  // Files
  "files.watcherExclude": {
    "**/.git/objects/**": true,
    "**/.git/subtree-cache/**": true,
    "**/build/**": true,
    "**/node_modules/**": true
  },
  "files.insertFinalNewline": true,
  "files.trimFinalNewlines": true,
  "files.trimTrailingWhitespace": true,

  // VSIcons
  "workbench.iconTheme": "vscode-icons",
  "vsicons.projectDetection.autoReload": true,
  "vsicons.dontShowNewVersionMessage": true,

  // GitLens
  "gitlens.views.repositories.files.layout": "auto",
  "gitlens.views.fileHistory.enabled": true,
  "gitlens.views.lineHistory.enabled": true,
  "gitlens.advanced.messages": {
    "suppressCommitHasNoPreviousCommitWarning": false,
    "suppressCommitNotFoundWarning": false,
    "suppressFileNotUnderSourceControlWarning": false,
    "suppressGitVersionWarning": false,
    "suppressLineUncommittedWarning": false,
    "suppressNoRepositoryWarning": false,
    "suppressResultsExplorerNotice": false,
    "suppressShowKeyBindingsNotice": true,
    "suppressUpdateNotice": true,
    "suppressWelcomeNotice": true
  },

  // Shell
  "shellcheck.useWorkspaceRootAsCwd": true,
  "shellcheck.customArgs": [
    //
    "--external-sources"
  ],

  // CSS
  "[css]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[less]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[scss]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },

  // Docker
  "[dockerfile]": {
    "editor.defaultFormatter": "ms-azuretools.vscode-docker"
  },

  // YAML
  "[yaml]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },

  // Javascript
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnPaste": false,
    "editor.formatOnSave": false
  },
  "[javascriptreact]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnPaste": false,
    "editor.formatOnSave": false
  },

  // JSON
  "[json]": {
    "editor.defaultFormatter": "vscode.json-language-features"
  },
  "[jsonc]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },

  // Typescript
  "[typescript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnPaste": false,
    "editor.formatOnSave": false
  },
  "[typescriptreact]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.formatOnPaste": false,
    "editor.formatOnSave": false
  },

  // MarkdownLint
  "markdownlint.config": {
    // Allow punctuation marks (?!) in headers
    "MD026": false
  },

  // Terraform
  "terraform.indexing": {
    "enabled": false, // Conflicts with terraform.languageServer
    "liveIndexing": false,
    "delay": 500,
    "exclude": [".terraform/**/*", "**/.terraform/**/*"]
  },
  "terraform.languageServer": {
    "enabled": true, // Required for 0.12
    "args": []
  },

  // Run on Save
  "runOnSave.commands": [
    //
    // {
    //   "match": "\\.(css|less|scss|sss)$",
    //   "command": "cd \"${workspaceFolder}\" && ./pipeline/generate-css-module \"${file}\"",
    //   "runIn": "backend",
    //   "runningStatusMessage": "Generating TS header for ${fileBasename}...",
    //   "finishStatusMessage": "TS header generated."
    // }
  ],

  // Editor - Code
  "editor.codeActionsOnSave": {
    "source.fixAll": true
  }
}
