
locals {
  empty_list         = []
  one_task_per_host  = ["distinctInstance"]
  az_balanced_spread = ["attribute:ecs.availability-zone", "instanceId"]
}

resource "aws_ecs_service" "service" {
  name = "${var.name}"

  task_definition                    = "${aws_ecs_task_definition.task_definition.family}:${aws_ecs_task_definition.task_definition.revision}"
  cluster                            = "${var.cluster_arn}"
  launch_type                        = "EC2"
  enable_ecs_managed_tags            = true
  scheduling_strategy                = "${var.scheduling_strategy}"
  desired_count                      = "${var.desired_count}"
  deployment_maximum_percent         = "${var.deployment_maximum_percent}"
  deployment_minimum_healthy_percent = "${var.deployment_minimum_healthy_percent}"
  health_check_grace_period_seconds  = "${var.health_check_grace_period_seconds}"

  dynamic "load_balancer" {
    for_each = "${lookup(var.load_balancer, "name", "") != "" ? list(var.load_balancer.container_port) : local.empty_list}"
    content {
      container_name   = "${var.prefix != "" ? var.prefix : var.name}"
      container_port   = "${load_balancer.value}"
      target_group_arn = "${aws_alb_target_group.alb_target_group.*.arn[0]}"
    }
  }

  dynamic "service_registries" {
    for_each = "${lookup(var.service_registries, "name", "") != "" ? list(var.service_registries.container_port) : local.empty_list}"
    content {
      container_name = "${var.prefix != "" ? var.prefix : var.name}"
      container_port = "${service_registries.value}"
      port           = 0
      registry_arn   = "${aws_service_discovery_service.service_discovery.*.arn[0]}"
    }
  }

  dynamic "ordered_placement_strategy" {
    for_each = "${var.task_placement == "AZBalancedSpread" ? local.az_balanced_spread : local.empty_list}"
    content {
      type  = "spread"
      field = "${ordered_placement_strategy.value}"
    }
  }

  dynamic "placement_constraints" {
    for_each = "${var.task_placement == "OneTaskPerHost" ? local.one_task_per_host : local.empty_list}"
    content {
      type = "${placement_constraints.value}"
    }
  }
}
